package Base;

import org.junit.Before;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.asserts.SoftAssert;

import java.util.List;
import java.util.concurrent.TimeUnit;

/**
 * Created by kislinskiy.a on 25.05.2016.
 */
public abstract class Utils extends BrowserFactory{



   SoftAssert softAssertion = new SoftAssert();

   public static void open(String url){
      openLink(url);
      driver.manage().window().maximize();
      deleteAllCookies();
   }

   public static void openLink(String url){
      driver.get(url);
   }


   public static void deleteAllCookies(){
      driver.manage().deleteAllCookies();
   }


   public static void implicitlyWait(int time){
      driver.manage().timeouts().implicitlyWait(time, TimeUnit.SECONDS);
   }

   public static void implicitlyScryptWait(int time){
      driver.manage().timeouts().setScriptTimeout(time, TimeUnit.SECONDS);
   }

   public static void implicitlyPageLoadWait(int time){
      driver.manage().timeouts().pageLoadTimeout(time, TimeUnit.SECONDS);
   }




   //Assertions
   public static void assertionByTitle(String titleText) {
      String gotTitleText = driver.getTitle();
      Assert.assertTrue(gotTitleText.contains(titleText), "Page has not downloaded completely or the title was changed");
   }

   public static void assertionByDisplayedText(WebElement displayedText){
      Assert.assertTrue(displayedText.isDisplayed());
   }


   public static void assertionBySourceContains(String expectedCode) {
      boolean pageSource = driver.getPageSource().contains(expectedCode);
      Assert.assertFalse(pageSource, "expectedCode is not displayed");
   }





   public static boolean deliveryChecker(String dropDownXpath, String message){
      boolean result=false;
      List<WebElement> list= driver.findElements(By.xpath(dropDownXpath));
      int dropdownSize = list.size();
      System.out.println("print dropdown");
	  for (int i = 0; i < dropdownSize; i++) {
        	System.out.println(list.get(i));}
      if (dropdownSize>5){
         result = true;
      }
      return result;
   }



   //WebElements
/*

   public void dropDownSelect(WebElement dropDwon, String visibleText) {
      Select select = new Select(dropDwon);
      select.selectByVisibleText(visibleText);
   }
*/



}
