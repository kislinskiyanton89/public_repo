package Base;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;

/**
 * Created by kislinskiy.a on 05.05.2016.
 */
public class BrowserFactory {
    static WebDriver driver;

    public static WebDriver startBrowser(String browserName){
        if(browserName.equals("firefox")){
            driver=new FirefoxDriver();
        }
        else if(browserName.equals("chrome")){
            driver = new ChromeDriver();
        }
        else if(browserName.equalsIgnoreCase("IE")){
            driver = new InternetExplorerDriver();
        }
       // driver.manage().window().maximize();


        return driver;

    }

}
