package LesterPages;

import Base.Utils;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

/**
 * Created by kislinskiy.a on 26.05.2016.
 */
public class OrderStepOne extends Utils {


    WebDriver driver;
    public OrderStepOne(WebDriver ldriver){
        this.driver=ldriver;

    }

    @FindBy(css = ".wSubmit.btn.btnCorner.btnGreen")
    WebElement proceedOrderButton;


    public void proceedOrderButtonClick(){
        proceedOrderButton.click();
    }






}