package LesterPages;

import Base.Utils;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

/**
 * Created by kislinskiy.a on 26.05.2016.
 */
public class OrderStepTwo extends Utils {

    WebDriver driver;
    public OrderStepTwo(WebDriver ldriver){
        this.driver=ldriver;

    }





    //Intime delivery
    @FindBy(xpath = ".//label[@for= 'it']")
    WebElement intimeCheckBox;


    @FindBy(css = ".dItemSubLabel>span")
    WebElement intimeDoSkladaRadio;


    @FindBy(xpath = "(.//span[@class='selection'])[1]")
    WebElement intimeDropDown;

    @FindBy(xpath = "//span[@class='select2-results']/ul/li[2]")
    WebElement intime_storage;


    //Nova Poshta delivery
    @FindBy(xpath = ".//label[@for= 'np']")
    WebElement npCheckBox;


    @FindBy(xpath = ".//input[@name='carrier_type1'][@value='0']")
    WebElement npDoSkladaRadio;


    @FindBy(id = "select2-address10-vk-container")
    WebElement npDropDown;


    //Samovivoz
    @FindBy(xpath = ".//label[@for= 'sam']")
    WebElement samovivozCheckBox;


    @FindBy(css = ".btn.btnGreen.btnCorner.cartNext.wSubmit")
    WebElement StepTwoProceedbutton;


    //Intime methods
    public void chooseIntimeDelivery(){
        if (!(intimeCheckBox.isSelected())){
            intimeCheckBox.click();
       }
    }

    public void chooseIntimeStorageDelivery(){
        intimeDoSkladaRadio.click();

    }

    public void chooseIntimeStorageNumber(){
        intimeDropDown.click();
        implicitlyWait(15);
        intime_storage.click();

    }



    public void checkIntimeAPI(){
        intimeCheckBox.click();
        intimeDoSkladaRadio.click();
        intimeDropDown.click();
        deliveryChecker("//span[@class='select2-results']/ul/li", "Intime delivery dropdown is not displayed!");

    }








    //NP methods

    public void checkNovaPoshtaAPI(){

        deliveryChecker("//select[@name='address00']//option", "NP delivery dropdown is not displayed!");

    }



    public void goToStepThree(){
        StepTwoProceedbutton.click();
    }



}
