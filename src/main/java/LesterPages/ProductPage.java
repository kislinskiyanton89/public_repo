package LesterPages;

import Base.Utils;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

/**
 * Created by kislinskiy.a on 25.05.2016.
 */
public class ProductPage extends Utils{
    WebDriver driver;


    public ProductPage(WebDriver ldriver){
        this.driver=ldriver;

    }



    //Header
    @FindBy(css = ".loginBtn.mfiA")
    WebElement loginButton;

    @FindBy(css = ".loginBtn")
    WebElement userMenuButton;

    @FindBy(xpath=".//div[@class='lkMenu']//ul//li[4]")
    WebElement logoutLink;


    //Authorization form
    @FindBy(xpath=".//*[@data-name='login']")
    WebElement phoneNumbderField;


    @FindBy(xpath=".//*[@data-name='password']")
    WebElement passwordField;


    @FindBy(xpath = "//*[text()='Войти']")
    WebElement enterButton;




    @FindBy(css=".btn.buyQuick.mfiA")
    WebElement quickOrderButton;

    @FindBy(css = "#backPhone")
    WebElement quickOrderPhoneNumderField;

    @FindBy(xpath = "//button[@class='wSubmit btn btnGreen']")
    WebElement quickOrderSendButton;

    @FindBy(css = ".btn.btnGreen.buyBtn.mfiB.microTranslate")
    WebElement buyButton;

    @FindBy(xpath = "//a[@class='btn btnGreen wb_order']" )
    WebElement makeOrderButton;



    public void userLogin(String phoneNumderAsLogin, String password){
        loginButton.click();
        implicitlyWait(3);
        enterButton.click();
        phoneNumbderField.sendKeys(phoneNumderAsLogin);
        passwordField.sendKeys(password);
        enterButton.click();

    }






    public void quickOrderPopupOpen(){
        quickOrderButton.click();

    }

    public void quickOrderFormSend(String quickOrderPhoneNumber){
        quickOrderSendButton.click();
        quickOrderPhoneNumderField.sendKeys(quickOrderPhoneNumber);
        quickOrderSendButton.click();
    }


    public void addToCart(){
        buyButton.click();
        implicitlyWait(15);
        makeOrderButton.click();
    }

    public boolean itemAvailabilityCheck(){
        boolean itemIsAvailable=true;
                if (driver.getPageSource().contains("нет в наличии")){
            itemIsAvailable = false;
        }
        return itemIsAvailable;

    }



}
