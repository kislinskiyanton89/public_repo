package LesterPages;

import Base.Utils;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

/**
 * Created by kislinskiy.a on 26.05.2016.
 */
public class OrderStepThree extends Utils {


    WebDriver driver;
    public OrderStepThree(WebDriver ldriver){
        this.driver=ldriver;

    }

    @FindBy(name = "comment")
    WebElement commentTextArea;


    @FindBy(css=".wSubmit.btn.btnCorner.btnGreen.wSubmit.Order")
    WebElement submitOrderButton;



    public void leaveComment(){
        commentTextArea.sendKeys("* Тестовый заказ *");
    }

    public void finishOrder(){
        submitOrderButton.click();
    }


}
