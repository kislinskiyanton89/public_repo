package LesterPages;

import Base.Utils;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

/**
 * Created by kislinskiy.a on 25.05.2016.
 */
public class HomePage extends Utils  {

    WebDriver driver;
    public HomePage(WebDriver ldriver){
        this.driver=ldriver;

    }


    //Header
    @FindBy(css = ".loginBtn.mfiA")
    WebElement loginButton;

    @FindBy(css = ".loginBtn")
    WebElement userMenuButton;

    @FindBy(xpath=".//div[@class='lkMenu']//ul//li[4]")
    WebElement logoutLink;



    //Authorization form
    @FindBy(xpath=".//*[@data-name='login']")
    WebElement phoneNumbderField;


    @FindBy(xpath=".//*[@data-name='password']")
    WebElement passwordField;


    @FindBy(xpath = "//*[text()='Войти']")
    WebElement enterButton;




    //Call back
    @FindBy(css = ".btn.btnRed.mfiA")
    WebElement callBackButton;

    @FindBy(css = "#backPhone")
    WebElement callBackPhoneNumberField;

    @FindBy(css = ".wSubmit.btn.btnGreen")
    WebElement callMeButton;

    @FindBy(css= ".wTxt>p")
    WebElement callBackSuccesPopup;



     public void userLogin(String phoneNumderAsLogin, String password){
        loginButton.click();
        enterButton.click();
        phoneNumbderField.sendKeys(phoneNumderAsLogin);
        passwordField.sendKeys(password);
        enterButton.click();

    }


    public void userLogout(){
        userMenuButton.click();
        implicitlyWait(3);
        logoutLink.click();
        }

    public void callBackCheck(String callBackPhoneNumber){
        callBackButton.click();
        callMeButton.click();
        callBackPhoneNumberField.sendKeys(callBackPhoneNumber);
        callMeButton.click();
    }

    public void assertCallBackSucces(){
        assertionByDisplayedText(callBackSuccesPopup);
    }

}
