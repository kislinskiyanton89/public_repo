import Base.BrowserFactory;
import Base.Utils;
import LesterPages.*;
import com.sun.xml.internal.bind.v2.TODO;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.util.concurrent.TimeUnit;

import static Base.Utils.*;

/**
 * Created by kislinskiy.a on 25.05.2016.
 */
public class LesterTests  {


    WebDriver driver = BrowserFactory.startBrowser("firefox");

    HomePage  homePage = PageFactory.initElements(driver, HomePage.class);
    ProductPage productPage = PageFactory.initElements(driver, ProductPage.class);
    OrderStepOne orderStepOne = PageFactory.initElements(driver, OrderStepOne.class);
    OrderStepTwo orderStepTwo = PageFactory.initElements(driver, OrderStepTwo.class);
    OrderStepThree orderStepThree = PageFactory.initElements(driver, OrderStepThree.class);




    String phoneNumderAsLogin="0955789631";
    String password="123456789";
    String callBackPhoneNumber = "0500000001";
    String quickOrderPhoneNumber ="0500000001";
    String productPageLink="http://lester.ua/195-60r15-hankook-leto-k425-88h-vengrija/p699";


    @AfterClass
    public void browserClose(){
        driver.quit();
}
    @BeforeClass
    public void Start(){
        open("http://lester.ua/");
    }



   @Test(priority = 1)
   public void loginLogoutCheck() throws InterruptedException{

       homePage.userLogin(phoneNumderAsLogin, password);
       assertionByTitle("Личный кабинет");  // это тот асершн о котором я говорил, берется из Utils
       Thread.sleep(2000);
       homePage.userLogout();

    }


    @Test(priority = 2)
    public void callBackCheck(){

        homePage.callBackCheck(callBackPhoneNumber);
       // homePage.assertCallBackSucces();   // переделать


    }

    @Test (priority = 3)
    public void quickOrderCheck(){
        openLink(productPageLink);
        implicitlyWait(3);
        productPage.quickOrderPopupOpen();
        productPage.quickOrderFormSend(quickOrderPhoneNumber);
    }




    @Test
    public void deliveryApiCheck(){
        productPage.userLogin(phoneNumderAsLogin, password);
        openLink(productPageLink);
        Assert.assertTrue(productPage.itemAvailabilityCheck(),"Item is unavailable");
        productPage.addToCart();
        orderStepOne.proceedOrderButtonClick();
        orderStepTwo.checkIntimeAPI(); //тут ошибка
        orderStepTwo.checkNovaPoshtaAPI();

    }

    @Test(priority = 4)
    public void orderCheck() throws InterruptedException {
        Thread.sleep(3000);
        productPage.userLogin(phoneNumderAsLogin, password);
        implicitlyScryptWait(6);
        openLink(productPageLink);
        implicitlyPageLoadWait(6);
        Assert.assertTrue(productPage.itemAvailabilityCheck(),"Item is unavailable");
        productPage.addToCart();
        orderStepOne.proceedOrderButtonClick();

        orderStepTwo.chooseIntimeDelivery();
        orderStepTwo.chooseIntimeStorageDelivery();
        orderStepTwo.chooseIntimeStorageNumber();
        orderStepTwo.goToStepThree();

        orderStepThree.leaveComment();
        orderStepThree.finishOrder();

    }



}
